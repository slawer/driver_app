package appsnmobilesolutions.view.calendarview;

/**
 * Created by Nilanchala on 9/8/15.
 */
public interface DayDecorator {
    void decorate(DayView cell);
}
