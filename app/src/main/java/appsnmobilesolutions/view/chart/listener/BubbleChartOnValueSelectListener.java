package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.BubbleValue;

public interface BubbleChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int bubbleIndex, BubbleValue value);

}
