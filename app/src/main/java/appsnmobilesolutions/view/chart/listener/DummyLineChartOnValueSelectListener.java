package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.PointValue;

public class DummyLineChartOnValueSelectListener implements LineChartOnValueSelectListener {

    @Override
    public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}
