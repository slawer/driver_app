package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.PointValue;
import appsnmobilesolutions.view.chart.model.SubcolumnValue;

public interface ComboLineColumnChartOnValueSelectListener extends OnValueDeselectListener {

    public void onColumnValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value);

    public void onPointValueSelected(int lineIndex, int pointIndex, PointValue value);

}
