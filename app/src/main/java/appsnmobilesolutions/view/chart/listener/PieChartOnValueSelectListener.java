package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.SliceValue;

public interface PieChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int arcIndex, SliceValue value);

}
