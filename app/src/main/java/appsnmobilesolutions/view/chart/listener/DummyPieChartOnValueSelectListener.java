package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.SliceValue;

public class DummyPieChartOnValueSelectListener implements PieChartOnValueSelectListener {

    @Override
    public void onValueSelected(int arcIndex, SliceValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}
