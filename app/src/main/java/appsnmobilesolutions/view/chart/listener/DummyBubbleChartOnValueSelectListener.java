package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.BubbleValue;

public class DummyBubbleChartOnValueSelectListener implements BubbleChartOnValueSelectListener {

    @Override
    public void onValueSelected(int bubbleIndex, BubbleValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}
