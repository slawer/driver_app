package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.SubcolumnValue;

public interface ColumnChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value);

}
