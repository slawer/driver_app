package appsnmobilesolutions.view.chart.listener;


import appsnmobilesolutions.view.chart.model.PointValue;

public interface LineChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int lineIndex, int pointIndex, PointValue value);

}
