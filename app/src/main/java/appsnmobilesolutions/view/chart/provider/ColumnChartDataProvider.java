package appsnmobilesolutions.view.chart.provider;

import appsnmobilesolutions.view.chart.model.ColumnChartData;

public interface ColumnChartDataProvider {

    public ColumnChartData getColumnChartData();

    public void setColumnChartData(ColumnChartData data);

}
