package appsnmobilesolutions.view.chart.provider;

import appsnmobilesolutions.view.chart.model.LineChartData;

public interface LineChartDataProvider {

    public LineChartData getLineChartData();

    public void setLineChartData(LineChartData data);

}
