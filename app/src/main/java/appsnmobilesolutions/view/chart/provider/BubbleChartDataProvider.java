package appsnmobilesolutions.view.chart.provider;

import appsnmobilesolutions.view.chart.model.BubbleChartData;

public interface BubbleChartDataProvider {

    public BubbleChartData getBubbleChartData();

    public void setBubbleChartData(BubbleChartData data);

}
