package appsnmobilesolutions.view.chart.provider;

import appsnmobilesolutions.view.chart.model.ComboLineColumnChartData;

public interface ComboLineColumnChartDataProvider {

    public ComboLineColumnChartData getComboLineColumnChartData();

    public void setComboLineColumnChartData(ComboLineColumnChartData data);

}
