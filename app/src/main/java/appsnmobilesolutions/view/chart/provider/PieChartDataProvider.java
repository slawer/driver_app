package appsnmobilesolutions.view.chart.provider;

import appsnmobilesolutions.view.chart.model.PieChartData;

public interface PieChartDataProvider {

    public PieChartData getPieChartData();

    public void setPieChartData(PieChartData data);

}
