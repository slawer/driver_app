package appsnmobilesolutions.view.chart.formatter;

import appsnmobilesolutions.view.chart.model.SliceValue;

public interface PieChartValueFormatter {

    public int formatChartValue(char[] formattedValue, SliceValue value);
}
