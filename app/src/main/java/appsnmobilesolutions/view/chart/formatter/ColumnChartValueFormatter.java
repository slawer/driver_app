package appsnmobilesolutions.view.chart.formatter;

import appsnmobilesolutions.view.chart.model.SubcolumnValue;

public interface ColumnChartValueFormatter {

    public int formatChartValue(char[] formattedValue, SubcolumnValue value);

}
