package appsnmobilesolutions.view.chart.formatter;


import appsnmobilesolutions.view.chart.model.PointValue;

public interface LineChartValueFormatter {

    public int formatChartValue(char[] formattedValue, PointValue value);
}
