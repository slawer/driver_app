package appsnmobilesolutions.view.chart.formatter;

import appsnmobilesolutions.view.chart.model.BubbleValue;

public interface BubbleChartValueFormatter {

    public int formatChartValue(char[] formattedValue, BubbleValue value);
}
