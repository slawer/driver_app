package appsnmobilesolutions.renaizance.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import appsnmobilesolutions.general.files.GeneralFunctions;
import appsnmobilesolutions.renaizance.driver.R;
import com.rengwuxian.materialedittext.MaterialEditText;

public class MomoPaymentActivity extends AppCompatActivity implements View.OnClickListener {

    public MaterialEditText edtMobileNumber, edtVoucherCode, edtAmount;
    public ImageButton imgBtnMTN, imgBtnTigo, imgBtnAirtel, imgBtnVodafone;
    public TextView textVodaInfo;

    Button btnProceedPayment;
    Toolbar toolbar;

    String network = "";
    String mob_number = "";
    Double amount;
    String voucher = "";

    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    java.util.Random rng; //Provide a seed if you want the same ones every time

    public GeneralFunctions generalFunc;

    String ride_amount = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_momo_payment);

        rng = new java.util.Random();

        generalFunc = new GeneralFunctions(MomoPaymentActivity.this);

        prefs = PreferenceManager.getDefaultSharedPreferences(MomoPaymentActivity.this);
        edit = prefs.edit();

        ride_amount = String.valueOf(prefs.getString("ride_total", "N/A"));

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.card_background));
        toolbar.setBackgroundColor(getResources().getColor(R.color.card_background));
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        edtMobileNumber = findViewById(R.id.edtMobileNumber);
        edtVoucherCode = findViewById(R.id.edtVoucherCode);
        edtAmount = findViewById(R.id.edtAmount);
        textVodaInfo = findViewById(R.id.vodaInfo);

        imgBtnAirtel = findViewById(R.id.btn_airtel);
        imgBtnTigo = findViewById(R.id.btn_tigo);
        imgBtnMTN = findViewById(R.id.btn_mtn);
        imgBtnVodafone = findViewById(R.id.btn_vodafone);

        btnProceedPayment = findViewById(R.id.btnProceedPayment);

        imgBtnTigo.setOnClickListener(this);
        imgBtnMTN.setOnClickListener(this);
        imgBtnAirtel.setOnClickListener(this);
        imgBtnVodafone.setOnClickListener(this);
        btnProceedPayment.setOnClickListener(this);

        edtAmount.setText(ride_amount);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_airtel:
                edtVoucherCode.setVisibility(View.GONE);
                textVodaInfo.setVisibility(View.GONE);
                network = "AIR";
                imgBtnAirtel.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                imgBtnVodafone.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnTigo.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnMTN.setBackgroundColor(getResources().getColor(android.R.color.white));
                break;

            case R.id.btn_mtn:
                edtVoucherCode.setVisibility(View.GONE);
                textVodaInfo.setVisibility(View.GONE);

                network = "MTN";
                imgBtnAirtel.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnVodafone.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnTigo.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnMTN.setBackgroundColor(getResources().getColor(R.color.color_mtn));
                break;

            case R.id.btn_tigo:
                edtVoucherCode.setVisibility(View.GONE);
                textVodaInfo.setVisibility(View.GONE);

                network = "TIG";
                imgBtnAirtel.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnVodafone.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnTigo.setBackgroundColor(getResources().getColor(R.color.color_tigo));
                imgBtnMTN.setBackgroundColor(getResources().getColor(android.R.color.white));
                break;

            case R.id.btn_vodafone:
                edtVoucherCode.setVisibility(View.VISIBLE);
                textVodaInfo.setVisibility(View.VISIBLE);

                network = "VOD";


                imgBtnAirtel.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnVodafone.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                imgBtnTigo.setBackgroundColor(getResources().getColor(android.R.color.white));
                imgBtnMTN.setBackgroundColor(getResources().getColor(android.R.color.white));
                break;

            case R.id.btnProceedPayment:
                //Toast.makeText(this, "Payment button works", Toast.LENGTH_SHORT).show();
                proceedWithPayment();
                break;
        }
    }




    private void proceedWithPayment() {
        mob_number = edtMobileNumber.getText().toString();
        voucher = edtVoucherCode.getText().toString();
        amount = Double.valueOf(edtAmount.getText().toString());


        if (mob_number.isEmpty() || String.valueOf(amount).isEmpty()) {
           // Toast.makeText(MomoPaymentActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
            showResponseDialog("All fields are required");
        }
        else if(network.isEmpty()){
            showResponseDialog("Kindly select your network provider");
            //Toast.makeText(MomoPaymentActivity.this, "Kindly select your network provider", Toast.LENGTH_SHORT).show();
        }
        else if (network.equals("VOD") && voucher.isEmpty() || (mob_number.isEmpty() || String.valueOf(amount).isEmpty())) {
            showResponseDialog("All fields are required");
            //Toast.makeText(MomoPaymentActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
        } else {

            Intent intent = new Intent(MomoPaymentActivity.this, ConfirmPaymentActivity.class);
            intent.putExtra("mobile_number", mob_number);
            intent.putExtra("voucher_code", voucher);
            intent.putExtra("amount", String.valueOf(amount));
            intent.putExtra("network", network);

//            Toast.makeText(this, ""+network, Toast.LENGTH_SHORT).show();
            startActivity(intent);


        }


    }

    public void showResponseDialog(String message) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(MomoPaymentActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(MomoPaymentActivity.this);

        builder.setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {


                            dialog.dismiss();

                        }
                );

        builder.setCancelable(false);
        builder.show();

    }

}
