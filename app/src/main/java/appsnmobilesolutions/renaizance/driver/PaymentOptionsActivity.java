package appsnmobilesolutions.renaizance.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import appsnmobilesolutions.general.files.ExecuteWebServerUrl;
import appsnmobilesolutions.general.files.GeneralFunctions;
import appsnmobilesolutions.renaizance.driver.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PaymentOptionsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton imgBtnCashPay;
    ImageButton imgBtnMomoPay;

    Toolbar toolbar;

    public GeneralFunctions generalFunc;

    java.util.Random rng;

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    String iCabRequestId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);

        rng = new java.util.Random();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setBackgroundColor(getResources().getColor(R.color.editBox_primary));
        toolbar.setTitle("Payment Options");
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_white);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        generalFunc = new GeneralFunctions(PaymentOptionsActivity.this);

        prefs = PreferenceManager.getDefaultSharedPreferences(PaymentOptionsActivity.this);
        edit = prefs.edit();

        iCabRequestId = String.valueOf(prefs.getString("iCabRequestId", "N/A"));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgBtnCashPay=findViewById(R.id.btnCashPayment);
        imgBtnMomoPay=findViewById(R.id.btnMomoPayment);

        imgBtnCashPay.setOnClickListener(this);
        imgBtnMomoPay.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCashPayment:
                updateRideWithPaymentRef();
                Intent intent = new Intent(PaymentOptionsActivity.this, TripRatingActivity.class);
                startActivity(intent);
                //this.finish();
                break;

            case R.id.btnMomoPayment:
                startActivity(new Intent(PaymentOptionsActivity.this,MomoPaymentActivity.class));
                break;
        }
    }

    public String generateUniqueID() {

        String genVal = "RN";
        long first14 = (rng.nextLong() % 100000000000000L);

        genVal = genVal + first14;

        return genVal.replace("-", "");
    }

    public void updateRideWithPaymentRef() {


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "updateRideWithPaymentRef");
        parameters.put("booking_payment_ref", "");
        parameters.put("iCabRequestId", iCabRequestId);
        parameters.put( "payment_mode","C");


        Log.d("params", String.valueOf(parameters));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(PaymentOptionsActivity.this, parameters);
        exeWebServer.setLoaderConfig(PaymentOptionsActivity.this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken", generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                try {
                    JSONObject jsonObject = new JSONObject(responseString);

                    if (responseString != null && !responseString.equals("")) {

                        //boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                        if (jsonObject.getString("resp_code").equals("000")) {


                        }


                        Log.d("MOMO Response", responseString);
                        //Log.d("Momo listLog", String.valueOf(messageArrayJSON));
                        showPaymentResponseDialog(jsonObject.getString("message"));
                    } else {
                        generalFunc.showError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        exeWebServer.execute();

    }

    private void showPaymentResponseDialog(String message) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

//                            Bundle bn = new Bundle();
//                            bn.putBundle("TRIP_DATA", data_trip);
//                            try {
//
//                                new StartActProcess(ConfirmPaymentActivity.this).startActWithData(TripRatingActivity.class, bn);
//
//                            } catch (Exception e) {
//                                new StartActProcess(ConfirmPaymentActivity.this).startActWithData(TripRatingActivity.class, bn);
//                            }
//
//                            Log.d("data_trip", String.valueOf(data_trip));
                            updateRideWithPaymentRef();
                            Intent intent = new Intent(PaymentOptionsActivity.this, TripRatingActivity.class);
//                            intent.putExtra("mobile_number", mob_number);
//                            intent.putExtra("voucher_code", voucher);
//                            intent.putExtra("amount", String.valueOf(amount));
                            startActivity(intent);
                            // finish();

                        }
                );

        builder.setCancelable(false);
        builder.show();

    }
}
