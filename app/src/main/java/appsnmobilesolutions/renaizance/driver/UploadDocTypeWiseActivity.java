package appsnmobilesolutions.renaizance.driver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import appsnmobilesolutions.general.files.GeneralFunctions;
import appsnmobilesolutions.general.files.StartActProcess;
import appsnmobilesolutions.renaizance.driver.R;
import appsnmobilesolutions.utils.CommonUtilities;
import appsnmobilesolutions.utils.Utils;
import appsnmobilesolutions.view.MTextView;

import appsnmobilesolutions.utils.CommonUtilities;
import appsnmobilesolutions.utils.Utils;
import appsnmobilesolutions.view.MTextView;

public class UploadDocTypeWiseActivity extends AppCompatActivity {


    LinearLayout uberxArea, deliverArea, rideArea;
    GeneralFunctions generalFunctions;
    MTextView ridetitleTxt, deliverytitleTxt, uberxtitleTxt;
    MTextView titleTxt;
    ImageView backImgView;

    public static int ADDVEHICLE = 1;

    int totalVehicles = 0;
    String app_type;
    String userProfileJson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_doc_type_wise);
        generalFunctions = new GeneralFunctions(getActContext());
        initView();
    }

    public void initView() {
        uberxArea = (LinearLayout) findViewById(R.id.uberxArea);
        rideArea = (LinearLayout) findViewById(R.id.rideArea);
        deliverArea = (LinearLayout) findViewById(R.id.deliverArea);
        ridetitleTxt = (MTextView) findViewById(R.id.ridetitleTxt);
        deliverytitleTxt = (MTextView) findViewById(R.id.deliverytitleTxt);
        uberxtitleTxt = (MTextView) findViewById(R.id.uberxtitleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        uberxArea.setOnClickListener(new setOnClickList());
        rideArea.setOnClickListener(new setOnClickList());
        deliverArea.setOnClickListener(new setOnClickList());
        backImgView.setOnClickListener(new setOnClickList());

        userProfileJson = generalFunctions.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
        app_type = generalFunctions.getJsonValue("APP_TYPE", userProfileJson);


        totalVehicles = getIntent().getIntExtra("totalVehicles", 0);

        if ((getIntent().getStringExtra("isChange") != null && getIntent().getStringExtra("isChange").equalsIgnoreCase("Yes"))) {
            uberxArea.setVisibility(View.GONE);
        }

        if (getIntent().getStringExtra("selView").equalsIgnoreCase("doc")) {
            ridetitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_UPLOAD_DOC_RIDE"));
            deliverytitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_UPLOAD_DOC_DELIVERY"));
            uberxtitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_UPLOAD_DOC_UFX"));
        } else {

            ridetitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_VEHICLES_RIDE"));
            deliverytitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_VEHICLES_DELIVERY"));
            uberxtitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_OTHER_SERVICES"));

        }
        titleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_SELECT_TYPE"));
    }

    public Context getActContext() {
        return UploadDocTypeWiseActivity.this;
    }


    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            Bundle bn = new Bundle();
            bn.putString("PAGE_TYPE", "Driver");
            bn.putString("iDriverVehicleId", "");
            bn.putString("doc_file", "");
            bn.putString("iDriverVehicleId", "");
            Utils.hideKeyboard(UploadDocTypeWiseActivity.this);
            switch (view.getId()) {
                case R.id.backImgView:
                    UploadDocTypeWiseActivity.super.onBackPressed();
                    break;

                case R.id.deliverArea:
                    bn.putString("seltype", "Delivery");
                    if (getIntent().getStringExtra("selView").equalsIgnoreCase("doc")) {
                        new StartActProcess(getActContext()).startActWithData(ListOfDocumentActivity.class, bn);
                    } else {
                        if (totalVehicles > 0) {
                            new StartActProcess(getActContext()).startActWithData(ManageVehiclesActivity.class, bn);
                        } else {
                            new StartActProcess(getActContext()).startActForResult(AddVehicleActivity.class, bn, ADDVEHICLE);
                        }
                    }
                    break;
                case R.id.uberxArea:

                    bn.putString("UBERX_PARENT_CAT_ID", getIntent().getStringExtra("UBERX_PARENT_CAT_ID"));
                    new StartActProcess(getActContext()).startActWithData(UfxCategoryActivity.class, bn);
                    break;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (data.getStringExtra("iDriverVehicleId") != null && !data.getStringExtra("iDriverVehicleId").equalsIgnoreCase
                    ("")) {
                totalVehicles = 1;

                ridetitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_VEHICLES_RIDE"));
                deliverytitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_VEHICLES_DELIVERY"));
                uberxtitleTxt.setText(generalFunctions.retrieveLangLBl("", "LBL_MANANGE_OTHER_SERVICES"));

            }
            //handle total vehicles
        }
    }
}
