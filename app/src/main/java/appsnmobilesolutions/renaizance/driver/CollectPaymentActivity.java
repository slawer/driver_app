package appsnmobilesolutions.renaizance.driver;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import appsnmobilesolutions.general.files.ExecuteWebServerUrl;
import appsnmobilesolutions.general.files.GeneralFunctions;
import appsnmobilesolutions.general.files.StartActProcess;
import appsnmobilesolutions.renaizance.driver.R;
import appsnmobilesolutions.utils.CommonUtilities;
import appsnmobilesolutions.utils.Utils;
import appsnmobilesolutions.view.ErrorView;
import appsnmobilesolutions.view.MButton;
import appsnmobilesolutions.view.MTextView;
import appsnmobilesolutions.view.MaterialRippleLayout;
import appsnmobilesolutions.view.editBox.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import appsnmobilesolutions.utils.CommonUtilities;
import appsnmobilesolutions.utils.Utils;
import appsnmobilesolutions.view.ErrorView;
import appsnmobilesolutions.view.MButton;
import appsnmobilesolutions.view.MTextView;
import appsnmobilesolutions.view.MaterialRippleLayout;
import appsnmobilesolutions.view.editBox.MaterialEditText;

public class CollectPaymentActivity extends AppCompatActivity {

    MTextView titleTxt;
    ImageView backImgView;

    GeneralFunctions generalFunc;

    ProgressBar loading;
    ErrorView errorView;
    MButton btn_type2;
    ImageView editCommentImgView;
    MTextView commentBox;
    MTextView generalCommentTxt;

    int submitBtnId;

    String appliedComment = "";
    LinearLayout container;
    LinearLayout fareDetailDisplayArea;


    RatingBar ratingBar;
    String iTripId_str;

    HashMap<String, String> data_trip;
    android.support.v7.app.AlertDialog collectPaymentFailedDialog = null;
    private View convertView = null;

    MTextView additionalchargeHTxt, matrialfeeHTxt, miscfeeHTxt, discountHTxt;
    MaterialEditText timatrialfeeVTxt, miscfeeVTxt, discountVTxt;
    MTextView matrialfeeCurrancyTxt, miscfeeCurrancyTxt, discountCurrancyTxt;
    ImageView discounteditImgView, miseeditImgView, matrialeditImgView;
    MTextView payTypeHTxt, dateVTxt;
    MTextView totalFareTxt, cartypeTxt;

    MTextView promoAppliedVTxt, promoAppliedTxt;

    String totalRidedifferenceSeconds3 = "";
    String TotalRideDistance1 = "";
    String TripTimeInMinutes = "";
    String fDistance = "";
    String DisplayDistanceTxt = "";
    String tSaddress = "";
    String tDaddress = "";
    String newveh_assigned_code = "";
    String newiVehicleSubDivTypeID = "";
    String iCabRequestId = "";
    String iUserId = "";

    String iPassengerName = "";


    public String pay_distance = "";
    public String pay_time = "";
    public String pay_time_fare = "";
    public String pay_base_fare = "";
    public String pay_distance_fare = "";
    public String estimated_total = "";

    String consumption_rate = "";
    String fuel_price = "";
    String maintenance_rate = "";
    String tyre_rate = "";
    String total_duration_mins = "";
    String time_constant = "";
    String distance_threshold = "";
    String percent_adj = "";
    String cartypename = "";
    String getTripId = "";

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    TextView txtDateT, txtBaseFare, txtDistance, txtDistanceFare, txtTime, txtTimeFare, txtSubtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_payment);


        generalFunc = new GeneralFunctions(getActContext());
        prefs = PreferenceManager.getDefaultSharedPreferences(CollectPaymentActivity.this);
        edit = prefs.edit();

        txtDateT = findViewById(R.id.txtDateTime);
        txtBaseFare = findViewById(R.id.txtBaseFare);
        txtDistance = findViewById(R.id.txtDistance);
        txtDistanceFare = findViewById(R.id.txtDistanceFare);
        txtTime = findViewById(R.id.txtTime);
        txtTimeFare = findViewById(R.id.txtTimeFare);

        txtSubtotal = findViewById(R.id.txtSubtotal);


        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        loading = (ProgressBar) findViewById(R.id.loading);
        errorView = (ErrorView) findViewById(R.id.errorView);
        editCommentImgView = (ImageView) findViewById(R.id.editCommentImgView);
        btn_type2 = ((MaterialRippleLayout) findViewById(R.id.btn_type2)).getChildView();
        commentBox = (MTextView) findViewById(R.id.commentBox);
        generalCommentTxt = (MTextView) findViewById(R.id.generalCommentTxt);
        container = (LinearLayout) findViewById(R.id.container);
        fareDetailDisplayArea = (LinearLayout) findViewById(R.id.fareDetailDisplayArea);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        payTypeHTxt = (MTextView) findViewById(R.id.payTypeHTxt);
        dateVTxt = (MTextView) findViewById(R.id.dateVTxt);
        promoAppliedVTxt = (MTextView) findViewById(R.id.promoAppliedVTxt);

        additionalchargeHTxt = (MTextView) findViewById(R.id.additionalchargeHTxt);
        matrialfeeHTxt = (MTextView) findViewById(R.id.matrialfeeHTxt);
        miscfeeHTxt = (MTextView) findViewById(R.id.miscfeeHTxt);
        discountHTxt = (MTextView) findViewById(R.id.discountHTxt);

        timatrialfeeVTxt = (MaterialEditText) findViewById(R.id.timatrialfeeVTxt);
        miscfeeVTxt = (MaterialEditText) findViewById(R.id.miscfeeVTxt);
        discountVTxt = (MaterialEditText) findViewById(R.id.discountVTxt);

        matrialfeeCurrancyTxt = (MTextView) findViewById(R.id.matrialfeeCurrancyTxt);
        miscfeeCurrancyTxt = (MTextView) findViewById(R.id.miscfeeCurrancyTxt);
        discountCurrancyTxt = (MTextView) findViewById(R.id.discountCurrancyTxt);

        discounteditImgView = (ImageView) findViewById(R.id.discounteditImgView);
        miseeditImgView = (ImageView) findViewById(R.id.miseeditImgView);
        matrialeditImgView = (ImageView) findViewById(R.id.matrialeditImgView);
        cartypeTxt = (MTextView) findViewById(R.id.cartypeTxt);

        totalFareTxt = (MTextView) findViewById(R.id.totalFareTxt);

        discounteditImgView.setOnClickListener(new setOnClickList());
        miscfeeCurrancyTxt.setOnClickListener(new setOnClickList());
        discountCurrancyTxt.setOnClickListener(new setOnClickList());


        timatrialfeeVTxt.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        // timatrialfeeVTxt.setImeOptions(EditorInfo.IME_ACTION_DONE);

        miscfeeVTxt.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        // miscfeeVTxt.setImeOptions(EditorInfo.IME_ACTION_DONE);

        discountVTxt.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
        // discountVTxt.setImeOptions(EditorInfo.IME_ACTION_DONE);

        discountVTxt.setShowClearButton(false);
        miscfeeVTxt.setShowClearButton(false);
        timatrialfeeVTxt.setShowClearButton(false);

        discountVTxt.addTextChangedListener(new setOnAddTextListner());
        miscfeeVTxt.addTextChangedListener(new setOnAddTextListner());
        timatrialfeeVTxt.addTextChangedListener(new setOnAddTextListner());

//        discountVTxt.setEnabled(false);
//        miscfeeVTxt.setEnabled(false);
//        timatrialfeeVTxt.setEnabled(false);


        submitBtnId = Utils.generateViewId();
        btn_type2.setId(submitBtnId);

        btn_type2.setOnClickListener(new setOnClickList());
        editCommentImgView.setOnClickListener(new setOnClickList());
        backImgView.setVisibility(View.GONE);
        setLabels();

        getFare();

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) titleTxt.getLayoutParams();
        params.setMargins(Utils.dipToPixels(getActContext(), 15), 0, 0, 0);
        titleTxt.setLayoutParams(params);

        data_trip = (HashMap<String, String>) getIntent().getSerializableExtra("TRIP_DATA");

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            String restratValue_str = savedInstanceState.getString("RESTART_STATE");

            if (restratValue_str != null && !restratValue_str.equals("") && restratValue_str.trim().equals("true")) {
                generalFunc.restartApp();
            }
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        outState.putString("RESTART_STATE", "true");
        super.onSaveInstanceState(outState);
    }

    public Context getActContext() {
        return CollectPaymentActivity.this;
    }

    public void setLabels() {
        titleTxt.setText(generalFunc.retrieveLangLBl("Your Trip", "LBL_PAY_SUMMARY"));
        commentBox.setText(generalFunc.retrieveLangLBl("", "LBL_ADD_COMMENT_TXT"));
        promoAppliedVTxt.setText(generalFunc.retrieveLangLBl("", "LBL_DIS_APPLIED"));
        btn_type2.setText(generalFunc.retrieveLangLBl("COLLECT PAYMENT", "LBL_COLLECT_PAYMENT"));
        ((MTextView) findViewById(R.id.detailsTxt)).setText(generalFunc.retrieveLangLBl("", "LBL_DETAILS"));


        additionalchargeHTxt.setText(generalFunc.retrieveLangLBl("ADDITIONAL CHARGES", "LBL_ADDITONAL_CHARGE_HINT"));
        matrialfeeHTxt.setText(generalFunc.retrieveLangLBl("Material fee", "LBL_MATERIAL_FEE"));
        miscfeeHTxt.setText(generalFunc.retrieveLangLBl("Misc fee", "LBL_MISC_FEE"));
        discountHTxt.setText(generalFunc.retrieveLangLBl("Provider Discount", "LBL_PROVIDER_DISCOUNT"));
        payTypeHTxt.setText("Payment type : ");
        dateVTxt.setText(generalFunc.retrieveLangLBl("", "LBL_MYTRIP_Trip_Date"));
        totalFareTxt.setText(generalFunc.retrieveLangLBl("", "LBL_Total_Fare"));
        discountVTxt.setText("0.0");
        miscfeeVTxt.setText("0.0");
        timatrialfeeVTxt.setText("0.0");


    }

    public void showCommentBox() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
        builder.setTitle(generalFunc.retrieveLangLBl("", "LBL_ADD_COMMENT_HEADER_TXT"));

        LayoutInflater inflater = (LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.input_box_view, null);
        builder.setView(dialogView);

        final MaterialEditText input = (MaterialEditText) dialogView.findViewById(R.id.editBox);

        input.setSingleLine(false);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setMaxLines(5);
        if (!appliedComment.equals("")) {
            input.setText(appliedComment);
        }
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Utils.getText(input).trim().equals("") && appliedComment.equals("")) {
                    generalFunc.showGeneralMessage("", generalFunc.retrieveLangLBl("", "LBL_ENTER_PROMO"));
                } else if (Utils.getText(input).trim().equals("") && !appliedComment.equals("")) {
                    appliedComment = "";
                    commentBox.setText(generalFunc.retrieveLangLBl("", "LBL_ADD_COMMENT_TXT"));
                    generalFunc.showGeneralMessage("", "Your comment has been removed.");
                } else {
                    appliedComment = Utils.getText(input);
                    commentBox.setText(appliedComment);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void getFare() {
        if (errorView.getVisibility() == View.VISIBLE) {
            errorView.setVisibility(View.GONE);
        }
        if (container.getVisibility() == View.VISIBLE) {
            container.setVisibility(View.GONE);
        }
        if (loading.getVisibility() != View.VISIBLE) {
            loading.setVisibility(View.VISIBLE);
        }

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "displayFare");
        parameters.put("iMemberId", generalFunc.getMemberId());
        parameters.put("UserType", CommonUtilities.app_type);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                Log.d("fare_tales", responseString);
                if (responseString != null && !responseString.equals("")) {

                    closeLoader();

                    if (generalFunc.checkDataAvail(CommonUtilities.action_str, responseString) == true) {

                        String message = generalFunc.getJsonValue(CommonUtilities.message_str, responseString);

                        String FormattedTripDate = generalFunc.getJsonValue("tTripRequestDateOrig", message);
                        String FareSubTotal = generalFunc.getJsonValue("FareSubTotal", message);
                        String eCancelled = generalFunc.getJsonValue("eCancelled", message);
                        String vCancelReason = generalFunc.getJsonValue("vCancelReason", message);
                        String vTripPaymentMode = generalFunc.getJsonValue("vTripPaymentMode", message);
                        String fDiscount = generalFunc.getJsonValue("fDiscount", message);
                        String CurrencySymbol = generalFunc.getJsonValue("CurrencySymbol", message);
                        cartypename = generalFunc.getJsonValue("carTypeName", message);

                        getTripId = generalFunc.getJsonValue("iTripId", message);

                        totalRidedifferenceSeconds3 = generalFunc.getJsonValue("TotalRidedifferenceSeconds3", message);
                        TotalRideDistance1 = generalFunc.getJsonValue("TotalRideDistance1", message);
                        TripTimeInMinutes = generalFunc.getJsonValue("TripTimeInMinutes", message);
                        fDistance = generalFunc.getJsonValue("fDistance", message);
                        DisplayDistanceTxt = generalFunc.getJsonValue("DisplayDistanceTxt", message);
                        tSaddress = generalFunc.getJsonValue("tSaddress", message);
                        tDaddress = generalFunc.getJsonValue("tDaddress", message);
                        newveh_assigned_code = generalFunc.getJsonValue("newveh_assigned_code", message);
                        newiVehicleSubDivTypeID = generalFunc.getJsonValue("newiVehicleSubDivTypeID", message);
                        iCabRequestId = generalFunc.getJsonValue("iCabRequestId", message);
                        iUserId = generalFunc.getJsonValue("iUserId", message);

                        String passsenerDetails = generalFunc.getJsonValue("PassengerDetails", message);

                        try {
                            JSONObject jsonObject = new JSONObject(passsenerDetails);

                            iPassengerName =jsonObject.getString("vName");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if (cartypename != null) {

                            cartypeTxt.setText(cartypename);

                            cartypeTxt.setVisibility(View.VISIBLE);
                        }

                        String iTripId = generalFunc.getJsonValue("iTripId", message);

                        iTripId_str = iTripId;


                        if (!fDiscount.equals("") && !fDiscount.equals("0") && !fDiscount.equals("0.00")) {

                            ((MTextView) findViewById(R.id.promoAppliedTxt)).setText(CurrencySymbol + generalFunc.convertNumberWithRTL(fDiscount));

                            (findViewById(R.id.promoView)).setVisibility(View.VISIBLE);
                        } else {

                            ((MTextView) findViewById(R.id.promoAppliedTxt)).setText("--");

                        }


                        String collectMoneytxt = "";
                        String deductedcard = "";
                        if (generalFunc.getJsonValue("eType", message).equals("Deliver")) {
                            dateVTxt.setText(generalFunc.retrieveLangLBl("", "LBL_DELIVERY_DATE_TXT"));
                            collectMoneytxt = generalFunc.retrieveLangLBl("Please collect money from rider", "LBL_COLLECT_MONEY_FRM_RECIPIENT");
                            deductedcard = generalFunc.retrieveLangLBl("", "LBL_DEDUCTED_SENDER_CARD");
                        } else {
                            dateVTxt.setText(generalFunc.retrieveLangLBl("", "LBL_TRIP_DATE_TXT"));
                            collectMoneytxt = generalFunc.retrieveLangLBl("Please collect money from rider", "LBL_COLLECT_MONEY_FRM_RIDER");
                            deductedcard = generalFunc.retrieveLangLBl("", "LBL_DEDUCTED_RIDER_CARD");
                        }


                            ((MTextView) findViewById(R.id.payTypeTxt)).setText(
                                    generalFunc.retrieveLangLBl("", "LBL_CASH_TXT") + "/Momo");

                            String pay_str = "";
                            if (Utils.getText(generalCommentTxt).length() > 0) {
                                pay_str = generalCommentTxt.getText().toString() + "\n" +
                                        collectMoneytxt;
                            } else {
                                pay_str = collectMoneytxt;
                            }
                            generalCommentTxt.setText(pay_str);
                            generalCommentTxt.setVisibility(View.VISIBLE);

//                            ((MTextView) findViewById(R.id.payTypeTxt)).setText(
//                                    generalFunc.retrieveLangLBl("", "LBL_CARD"));
//                            generalCommentTxt.setText(deductedcard);
//                            generalCommentTxt.setVisibility(View.VISIBLE);


//                        ((MTextView) findViewById(R.id.dateTxt)).setText(generalFunc.getDateFormatedType(FormattedTripDate, Utils.OriginalDateFormate, Utils.dateFormateInList));
                        ((MTextView) findViewById(R.id.dateTxt)).setText(generalFunc.getDateFormatedType(FormattedTripDate, Utils.OriginalDateFormate, Utils.DateFormateInDetailScreen));


                        container.setVisibility(View.VISIBLE);
                        boolean FareDetailsArrNew = generalFunc.isJSONkeyAvail("FareDetailsNewArr", responseString);

                        JSONArray FareDetailsArrNewObj = null;
                        if (FareDetailsArrNew == true) {
                            FareDetailsArrNewObj = generalFunc.getJsonArray("FareDetailsNewArr", responseString);
                        }
                        if (FareDetailsArrNewObj != null)
                            addFareDetailLayout(FareDetailsArrNewObj);

                    } else {
                        generateErrorView();
                    }
                } else {
                    generateErrorView();
                }


                calculateFee(newveh_assigned_code, TotalRideDistance1, newiVehicleSubDivTypeID, totalRidedifferenceSeconds3, TripTimeInMinutes, fDistance, iCabRequestId, iUserId);
            }
        });
        exeWebServer.execute();
    }

    public void calculateFee(String a_code, String total_ride_distance, String v_sub_type_div, String _time_sec, String _time_minutes, String f_distance, String cabReq, String pass_id) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "computePrice");
        parameters.put("isKilometer", "1");
        parameters.put("assigned_code", a_code);
        parameters.put("vehicle_sub_type_div", v_sub_type_div);
        parameters.put("minutes", _time_sec);
        parameters.put("kilometers", total_ride_distance);
        parameters.put("iUserId", generalFunc.getMemberId());
        parameters.put("TripTimeInMinutes", _time_minutes);
        parameters.put("fDistance", f_distance);
        parameters.put("save_compute_price", "1");
        parameters.put("iCabRequestId", iCabRequestId);
        parameters.put("ride_event", "E");
        parameters.put("passengerID", pass_id);
        parameters.put("vDeviceType", Utils.deviceType);


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(CollectPaymentActivity.this, parameters);
        exeWebServer.setLoaderConfig(CollectPaymentActivity.this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken", generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    Log.d("ass_code", responseString);
                    if (responseString != null && !responseString.equals("")) {
                        //boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);
                        if (jsonObject.getString("resp_code").equals("100")) {

//                            pay_distance = jsonObject.getString("distance");
//                            pay_time = jsonObject.getString("time");
//                            pay_time_fare = jsonObject.getString("time_fare");
//                            pay_base_fare = jsonObject.getString("base_rate");
//                            pay_distance_fare = jsonObject.getString("dist_price");
//                            estimated_total = jsonObject.getString("final_amount");

                            pay_distance = jsonObject.getString("distance");
                            pay_time = jsonObject.getString("time");
                            pay_time_fare = jsonObject.getString("time_fare");
                            pay_base_fare = jsonObject.getString("base_rate");
                            pay_distance_fare = jsonObject.getString("dist_price");
                            estimated_total = jsonObject.getString("the_total");
                            consumption_rate = jsonObject.getString("consumption_rate");
                            fuel_price = jsonObject.getString("fuel_price");
                            maintenance_rate = jsonObject.getString("maintenance_rate");
                            tyre_rate = jsonObject.getString("tyre_rate");
                            total_duration_mins = jsonObject.getString("total_duration_mins");
                            time_constant = jsonObject.getString("time_constant");
                            distance_threshold = jsonObject.getString("distance_threshold");
                            percent_adj = jsonObject.getString("percent_adj");


                        }


                    } else {
                        generalFunc.showError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Log.d("pay_distance", pay_distance);
//                Log.d("pay_distance_fare", pay_distance_fare);
//                Log.d("pay_distance_fare", time);
//                Log.d("pay_distance_fare", pay_distance_fare);
//                displayDetails();
//                totalFareTxt.setText("¢"+estimated_total);
                ((MTextView) findViewById(R.id.fareTxt)).setText(String.format("¢%s", generalFunc.convertNumberWithRTL(estimated_total)));

                txtBaseFare.setText(String.format("¢%s", pay_base_fare));

                txtDistance.setText("Distance:");

                txtDistanceFare.setText(String.format("%s km", pay_distance));

                txtTime.setText("Time:");
                txtTimeFare.setText(String.format("%s minutes", pay_time));

                txtSubtotal.setText(String.format("¢%s", estimated_total));
            }
        });
        exeWebServer.execute();


    }

    private void addFareDetailLayout(JSONArray jobjArray) {

        if (fareDetailDisplayArea.getChildCount() > 0) {
            fareDetailDisplayArea.removeAllViewsInLayout();
        }

        for (int i = 0; i < jobjArray.length(); i++) {
            JSONObject jobject = generalFunc.getJsonObject(jobjArray, i);
            try {
                addFareDetailRow(jobject.names().getString(0), jobject.get(jobject.names().getString(0)).toString(), false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void addFareDetailRow(String row_name, String row_value, boolean isLast) {
        LayoutInflater infalInflater = (LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.design_fare_deatil_row, null);
        TableRow FareDetailRow = (TableRow) convertView.findViewById(R.id.FareDetailRow);
        TableLayout fair_area_table_layout = (TableLayout) convertView.findViewById(R.id.fair_area);
        MTextView titleHTxt = (MTextView) convertView.findViewById(R.id.titleHTxt);
        MTextView titleVTxt = (MTextView) convertView.findViewById(R.id.titleVTxt);

        titleHTxt.setText(generalFunc.convertNumberWithRTL(row_name));
        titleVTxt.setText(generalFunc.convertNumberWithRTL(row_value));

        if (isLast == true) {
            TableLayout.LayoutParams tableRowParams =
                    new TableLayout.LayoutParams
                            (TableLayout.LayoutParams.FILL_PARENT, 40);
            tableRowParams.setMargins(0, 10, 0, 0);

            fair_area_table_layout.setLayoutParams(tableRowParams);
            FareDetailRow.setLayoutParams(tableRowParams);
            fair_area_table_layout.setBackgroundColor(Color.parseColor("#EBEBEB"));
            fair_area_table_layout.getChildAt(0).setPadding(10, 0, 10, 0);
        } else {
            titleHTxt.setTextColor(Color.parseColor("#303030"));
            titleVTxt.setTextColor(Color.parseColor("#111111"));
        }
        if (convertView != null)
            fareDetailDisplayArea.addView(convertView);
    }

    public void collectPayment(String isCollectCash) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "CollectPayment");
        parameters.put("iTripId", iTripId_str);
        if (!isCollectCash.equals("")) {
            parameters.put("isCollectCash", isCollectCash);
        }

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        Bundle bn = new Bundle();
                        bn.putSerializable("TRIP_DATA", data_trip);
                        try {
                            if (data_trip.get("eHailTrip").equalsIgnoreCase("Yes")) {
                                generalFunc.restartwithGetDataApp();

                            } else {
                                new StartActProcess(getActContext()).startActWithData(TripRatingActivity.class, bn);
                            }
                        } catch (Exception e) {
                            new StartActProcess(getActContext()).startActWithData(TripRatingActivity.class, bn);
                        }

                    } else {
//                        buildPaymentCollectFailedMessage(generalFunc.retrieveLangLBl("",
//                                generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));

                    }
                } else {
                    generalFunc.showError();
                }
            }

        });
        exeWebServer.execute();
        Log.d("data_trip", String.valueOf(data_trip));
    }

//    public void buildPaymentCollectFailedMessage(String msg) {
//
//        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext(), R.style.StackedAlertDialogStyle);
//        builder.setTitle("");
//        builder.setCancelable(false);
//
//        builder.setMessage(msg);
//
//        builder.setPositiveButton(generalFunc.retrieveLangLBl("", "LBL_RETRY_TXT"), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                collectPaymentFailedDialog.dismiss();
//                collectPayment("");
//            }
//        });
//        builder.setNegativeButton(generalFunc.retrieveLangLBl("Collect Cash", "LBL_COLLECT_CASH"), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                collectPaymentFailedDialog.dismiss();
//                collectPayment("true");
//            }
//        });
//
//        collectPaymentFailedDialog = builder.create();
//        collectPaymentFailedDialog.setCancelable(false);
//        collectPaymentFailedDialog.setCanceledOnTouchOutside(false);
//        collectPaymentFailedDialog.show();
//    }

    public void closeLoader() {
        if (loading.getVisibility() == View.VISIBLE) {
            loading.setVisibility(View.GONE);
        }
    }

    public void generateErrorView() {

        closeLoader();

        generalFunc.generateErrorView(errorView, "LBL_ERROR_TXT", "LBL_NO_INTERNET_TXT");

        if (errorView.getVisibility() != View.VISIBLE) {
            errorView.setVisibility(View.VISIBLE);
        }
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getFare();
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int i = view.getId();
            Utils.hideKeyboard(CollectPaymentActivity.this);
            if (i == submitBtnId) {

                showPaymentResponseDialog();
                //collectPayment("");
//                Intent intent= new Intent(CollectPaymentActivity.this,PaymentOptionsActivity.class);
//                startActivity(intent);
            } else if (i == editCommentImgView.getId()) {
                showCommentBox();
            } else if (i == discounteditImgView.getId()) {
                discountVTxt.setEnabled(true);

            } else if (i == miscfeeCurrancyTxt.getId()) {
                miscfeeVTxt.setEnabled(true);
            } else if (i == discountCurrancyTxt.getId()) {
                timatrialfeeVTxt.setEnabled(true);

            }


        }
    }

    private void showPaymentResponseDialog() {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage("Proceed to collect payment?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    edit.putString("pay_base_fare", pay_base_fare);
                    edit.putString("ride_total", estimated_total);
                    edit.putString("pay_time", pay_time);
                    edit.putString("ride_status", "3");
                    edit.putString("act_status", "1");
                    edit.putString("ride_event", "E");
                    edit.putString("pay_distance", pay_distance);
                    edit.putString("consumption_rate", consumption_rate);
                    edit.putString("fuel_price", fuel_price);
                    edit.putString("maintenance_rate", maintenance_rate);
                    edit.putString("tyre_rate", tyre_rate);
                    edit.putString("total_duration_mins", total_duration_mins);
                    edit.putString("time_constant", time_constant);
                    edit.putString("distance_threshold", distance_threshold);
                    edit.putString("percent_adj", percent_adj);
                    edit.putString("getTripId", getTripId);

                    edit.putString("iCabRequestId", iCabRequestId);

                    edit.putString("source_address", tSaddress);
                    edit.putString("dest_address", tDaddress);
                    edit.putString("cartypename", cartypename);
                    edit.putString("iPassengerName", iPassengerName);
                    edit.commit();

                    Intent intent = new Intent(CollectPaymentActivity.this, PaymentOptionsActivity.class);
                    startActivity(intent);
                        }
                )
                .setNegativeButton("No", (dialog, which) -> {
                    dialog.dismiss();
                });

        builder.setCancelable(false);
        builder.show();

    }


    public class setOnAddTextListner implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
