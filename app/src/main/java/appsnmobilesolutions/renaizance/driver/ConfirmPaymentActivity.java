package appsnmobilesolutions.renaizance.driver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import appsnmobilesolutions.general.files.ExecuteWebServerUrl;
import appsnmobilesolutions.general.files.GeneralFunctions;
import appsnmobilesolutions.renaizance.driver.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ConfirmPaymentActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView txtCarType, txtPickupLocation, txtDistinationLocation;
    TextView txtDateTime, txtPhoneNumber, txtNetwork, txtAmount, txtVoucherCode, txtFareLarge;

    Button btnConfirmPayment;

    String selectedcar = "";
    String iUserId = "";
    String distance = "";
    String time = "";
    String PromoCode = "";
    String vVehicleType = "";
    String assigned_code = "";
    String pickup_add = "";
    String dest_add = "";

    String network = "";
    String mob_number = "";
    String amount = "";
    String voucher = "";

    String source = "";
    String destination = "";
    String ride_amount = "";
    String cartypename = "";

    String uniqueId = "";
    String iCabRequestId = "";


    //HashMap<String, String> data_trip;

    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    public GeneralFunctions generalFunc;

    java.util.Random rng; //Provide a seed if you want the same ones every time

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_payment);

        registerComponents();
    }

    public void registerComponents() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Confirm Payment");
        toolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        toolbar.setTitleTextColor(getResources().getColor(R.color.card_background));
        toolbar.setBackgroundColor(getResources().getColor(R.color.editBox_primary));
//        toolbar.setNavigationIcon(R.drawable.ic_arrow_white);
        setSupportActionBar(toolbar);

        prefs = PreferenceManager.getDefaultSharedPreferences(ConfirmPaymentActivity.this);
        edit = prefs.edit();

        rng = new java.util.Random();
        generalFunc = new GeneralFunctions(ConfirmPaymentActivity.this);

        source = String.valueOf(prefs.getString("source_address", "N/A"));
        destination = String.valueOf(prefs.getString("dest_address", "N/A"));
        ride_amount = String.valueOf(prefs.getString("ride_total", "N/A"));
        cartypename = String.valueOf(prefs.getString("cartypename", "N/A"));
        iCabRequestId = String.valueOf(prefs.getString("iCabRequestId", "N/A"));

//        data_trip = (HashMap<String, String>) getIntent().getSerializableExtra("TRIP_DATA");

        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            mob_number = extras.getString("mobile_number");
            voucher = extras.getString("voucher_code");
            amount = extras.getString("amount");
            network = extras.getString("network");
        }


//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        txtCarType = findViewById(R.id.txtCarType);
        txtPickupLocation = findViewById(R.id.txtPickupLocation);
        txtDistinationLocation = findViewById(R.id.txtDistinationLocation);

        btnConfirmPayment = findViewById(R.id.btnConfirmPayment);

        btnConfirmPayment.setOnClickListener(this);

        txtDateTime = findViewById(R.id.txtDateTime);
        txtAmount = findViewById(R.id.txtAmount);
        txtFareLarge = findViewById(R.id.txtFareLarge);
        txtNetwork = findViewById(R.id.txtNetwork);
        txtVoucherCode = findViewById(R.id.txtVoucherCode);
        txtPhoneNumber = findViewById(R.id.txtPhoneNumber);

        displayDetails();
    }

    public String generateUniqueID() {

        String genVal = "RN";
        long first14 = (rng.nextLong() % 100000000000000L);

        genVal = genVal + first14;

        return genVal.replace("-", "");
    }


    public void displayDetails() {
        Calendar c = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
        String timeFormat = dateFormat.format(new Date()).toString();

        String newNetwork = "";


        txtCarType.setText(cartypename);
        txtDateTime.setText(String.format("%s %s", currentDate, timeFormat));
        txtPhoneNumber.setText(mob_number);
        txtVoucherCode.setText(voucher);
        txtAmount.setText(String.format("¢%s", amount));


        txtPickupLocation.setText(source);
        txtDistinationLocation.setText(destination);

        txtFareLarge.setText(String.format("¢%s", amount));

        switch (network) {
            case "MTN":
                newNetwork = "MTN";

                break;
            case "TIG":
                newNetwork = "Tigo";

                break;
            case "AIR":
                newNetwork = "Airtel";
                txtNetwork.setText(newNetwork);
                break;
            case "VOD":
                newNetwork = "Vodafone";

                break;


        }
        txtNetwork.setText(newNetwork);

        //Toast.makeText(this, "" + newNetwork, Toast.LENGTH_SHORT).show();

    }

    public void makePaymentRequest() {

        uniqueId = generateUniqueID();


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "makePayment");
        parameters.put("customer_number", mob_number);
        parameters.put("amount", String.valueOf(amount));
        parameters.put("exttrid", uniqueId);
        parameters.put("nw", network);
        parameters.put("voucher_code", voucher);


        Log.d("params", String.valueOf(parameters));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(ConfirmPaymentActivity.this, parameters);
        exeWebServer.setLoaderConfig(ConfirmPaymentActivity.this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken", generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                try {
                    JSONObject jsonObject = new JSONObject(responseString);

                    if (responseString != null && !responseString.equals("")) {

                        //boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                        if (jsonObject.getString("resp_code").equals("000")) {


                        }


                        Log.d("MOMO Response", responseString);
                        //Log.d("Momo listLog", String.valueOf(messageArrayJSON));
                        showPaymentResponseDialog(jsonObject.getString("message"));
                    } else {
                        generalFunc.showError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        exeWebServer.execute();

    }


    private void showPaymentConfirmationDialog() {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to proceed with payment?")
                .setPositiveButton("Yes", (dialog, which) -> {


                            makePaymentRequest();


                        }
                )
                .setNegativeButton("No", (dialog, which) -> {
                    dialog.dismiss();
                });

        builder.setCancelable(false);
        builder.show();

    }


    private void showPaymentResponseDialog(String message) {
        android.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        else
            builder = new android.app.AlertDialog.Builder(this);

        builder.setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

//                            Bundle bn = new Bundle();
//                            bn.putBundle("TRIP_DATA", data_trip);
//                            try {
//
//                                new StartActProcess(ConfirmPaymentActivity.this).startActWithData(TripRatingActivity.class, bn);
//
//                            } catch (Exception e) {
//                                new StartActProcess(ConfirmPaymentActivity.this).startActWithData(TripRatingActivity.class, bn);
//                            }
//
//                            Log.d("data_trip", String.valueOf(data_trip));
                            updateRideWithPaymentRef();
                            Intent intent = new Intent(ConfirmPaymentActivity.this, TripRatingActivity.class);
//                            intent.putExtra("mobile_number", mob_number);
//                            intent.putExtra("voucher_code", voucher);
//                            intent.putExtra("amount", String.valueOf(amount));
                            startActivity(intent);
                            // finish();

                        }
                );

        builder.setCancelable(false);
        builder.show();

    }

    public void updateRideWithPaymentRef() {


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "updateRideWithPaymentRef");
        parameters.put("booking_payment_ref", uniqueId);
        parameters.put("iCabRequestId", iCabRequestId);
        parameters.put( "payment_mode","M");


        Log.d("params", String.valueOf(parameters));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(ConfirmPaymentActivity.this, parameters);
        exeWebServer.setLoaderConfig(ConfirmPaymentActivity.this, true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken", generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                try {
                    JSONObject jsonObject = new JSONObject(responseString);

                    if (responseString != null && !responseString.equals("")) {

                        //boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                        if (jsonObject.getString("resp_code").equals("000")) {


                        }


                        Log.d("MOMO Response", responseString);
                        //Log.d("Momo listLog", String.valueOf(messageArrayJSON));
                        showPaymentResponseDialog(jsonObject.getString("message"));
                    } else {
                        generalFunc.showError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        exeWebServer.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirmPayment:
                showPaymentConfirmationDialog();

                break;
        }
    }
}
