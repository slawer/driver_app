package appsnmobilesolutions.pubnub.api.enums;


public enum PNHeartbeatNotificationOptions {

    NONE,
    FAILURES,
    ALL

}
