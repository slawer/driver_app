package appsnmobilesolutions.pubnub.api.enums;

public enum PNPushType {

    APNS,
    MPNS,
    GCM

}
