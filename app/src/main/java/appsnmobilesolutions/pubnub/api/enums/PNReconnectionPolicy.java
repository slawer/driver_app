package appsnmobilesolutions.pubnub.api.enums;

public enum PNReconnectionPolicy {

    NONE,
    LINEAR,
    EXPONENTIAL
}
