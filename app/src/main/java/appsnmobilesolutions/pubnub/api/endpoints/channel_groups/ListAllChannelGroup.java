package appsnmobilesolutions.pubnub.api.endpoints.channel_groups;

import appsnmobilesolutions.pubnub.api.PubNub;
import appsnmobilesolutions.pubnub.api.PubNubException;
import appsnmobilesolutions.pubnub.api.builder.PubNubErrorBuilder;
import appsnmobilesolutions.pubnub.api.endpoints.Endpoint;
import appsnmobilesolutions.pubnub.api.enums.PNOperationType;
import appsnmobilesolutions.pubnub.api.managers.RetrofitManager;
import appsnmobilesolutions.pubnub.api.managers.TelemetryManager;
import appsnmobilesolutions.pubnub.api.models.consumer.channel_group.PNChannelGroupsListAllResult;
import appsnmobilesolutions.pubnub.api.models.server.Envelope;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.experimental.Accessors;
import retrofit2.Call;
import retrofit2.Response;

@Accessors(chain = true, fluent = true)
public class ListAllChannelGroup extends Endpoint<Envelope<Object>, PNChannelGroupsListAllResult> {

    public ListAllChannelGroup(PubNub pubnub, TelemetryManager telemetryManager, RetrofitManager retrofit) {
        super(pubnub, telemetryManager, retrofit);
    }

    @Override
    protected List<String> getAffectedChannels() {
        return null;
    }

    @Override
    protected List<String> getAffectedChannelGroups() {
        return null;
    }

    @Override
    protected void validateParams() throws PubNubException {
    }

    @Override
    protected Call<Envelope<Object>> doWork(Map<String, String> params) {
        return this.getRetrofit().getChannelGroupService()
                .listAllChannelGroup(this.getPubnub().getConfiguration().getSubscribeKey(), params);
    }

    @Override
    protected PNChannelGroupsListAllResult createResponse(Response<Envelope<Object>> input) throws PubNubException {
        Map<String, Object> stateMappings;

        if (input.body() == null || input.body().getPayload() == null) {
            throw PubNubException.builder().pubnubError(PubNubErrorBuilder.PNERROBJ_PARSING_ERROR).build();
        }

        stateMappings = (Map<String, Object>) input.body().getPayload();
        List<String> groups = (ArrayList<String>) stateMappings.get("groups");

        return PNChannelGroupsListAllResult.builder()
                .groups(groups)
                .build();
    }

    @Override
    protected PNOperationType getOperationType() {
        return PNOperationType.PNChannelGroupsOperation;
    }

    @Override
    protected boolean isAuthRequired() {
        return true;
    }

}
