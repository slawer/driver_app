package appsnmobilesolutions.pubnub.api.callbacks;

import appsnmobilesolutions.pubnub.api.PubNub;
import appsnmobilesolutions.pubnub.api.models.consumer.PNStatus;
import appsnmobilesolutions.pubnub.api.models.consumer.pubsub.PNMessageResult;
import appsnmobilesolutions.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

public abstract class SubscribeCallback {
    public abstract void status(PubNub pubnub, PNStatus status);

    public abstract void message(PubNub pubnub, PNMessageResult message);

    public abstract void presence(PubNub pubnub, PNPresenceEventResult presence);
}
