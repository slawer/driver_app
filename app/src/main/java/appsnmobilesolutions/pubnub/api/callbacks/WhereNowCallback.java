package appsnmobilesolutions.pubnub.api.callbacks;

import appsnmobilesolutions.pubnub.api.models.consumer.presence.PNWhereNowResult;


public abstract class WhereNowCallback extends PNCallback<PNWhereNowResult> {
}
