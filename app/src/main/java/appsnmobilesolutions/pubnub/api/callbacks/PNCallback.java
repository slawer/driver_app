package appsnmobilesolutions.pubnub.api.callbacks;


import appsnmobilesolutions.pubnub.api.models.consumer.PNStatus;

public abstract class PNCallback<X> {
    public abstract void onResponse(X result, PNStatus status);
}

